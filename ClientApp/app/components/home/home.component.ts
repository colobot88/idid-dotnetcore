import { Component, Input } from '@angular/core';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent {

    lat: number = 51.678418;
    lng: number = 7.809007;

    onPlaceSelect(place: any) {
        console.log(place);
        this.lat = place.geometry.location.lat();
        this.lng = place.geometry.location.lng();
    }
    
}
