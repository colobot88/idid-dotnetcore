﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AutoCompleteModule } from '../autocomplete/autocomplete.module';
import { ToolbarComponent } from './toolbar.component';

@NgModule({
    imports: [CommonModule, FormsModule, AutoCompleteModule],
    exports: [ToolbarComponent],
    declarations: [ToolbarComponent],
    providers: []
})
export class ToolbarModule { }
