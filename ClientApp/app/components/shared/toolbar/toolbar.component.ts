﻿import { Component, Input, Output, ElementRef, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'idid-toolbar',
    templateUrl: 'toolbar.component.html',
    styleUrls: ['toolbar.component.css']
})
export class ToolbarComponent {

    @Output('on-place-select') onPlaceSelect: EventEmitter<any> = new EventEmitter();

    lat: number = 51.678418;
    lng: number = 7.809007;
    loggedin: boolean = false;

    constructor(private router: Router) {
    }

    navigate(route: string) {
        this.router.navigate([route]);
    }

    onPlaceSelected(place: any) {
        console.log(place);
        this.onPlaceSelect.emit(place);
    }

    loginWithFacebook() {
        this.loggedin = true;
    }

    logout() {
        this.loggedin = false;
        this.router.navigate(['home']);
    }
}
