﻿import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AutoCompleteModule } from './autocomplete/autocomplete.module';
import { AutoCompleteComponent } from './autocomplete/autocomplete.component';
import { GoogleMapsModule } from './google-maps/google-maps.module';
import { GoogleMapsComponent } from './google-maps/google-maps.component';
import { ToolbarModule } from './toolbar/toolbar.module';
import { ToolbarComponent } from './toolbar/toolbar.component';

@NgModule({
    imports: [
        CommonModule, FormsModule, RouterModule, AutoCompleteModule, GoogleMapsModule, ToolbarModule
    ],
    declarations: [
        // COMPONENTS
        // PIPES
    ],
    providers: [
        // FILTERS
        // PIPES
        // SERVICES
    ],
    exports: [
        CommonModule, FormsModule, RouterModule,
        // COMPONENTS
        AutoCompleteComponent, GoogleMapsComponent, ToolbarComponent
    ]
})

export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule
        };
    }
}