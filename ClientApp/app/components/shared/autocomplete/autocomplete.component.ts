﻿import { Component, Input, Output, ElementRef, EventEmitter, ViewChild, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';

import {
    AgmCoreModule,
    MapsAPILoader
} from '@agm/core';

declare var google: any;


@Component({
    selector: 'idid-autocomplete',
    host: {
        '(document:click)': 'handleClick($event)',
        '(keydown)': 'handleBlur($event)',
        '(document:keydown)': 'keyDown($event)'
    },
    templateUrl: 'autocomplete.component.html',
    styleUrls: ['./autocomplete.component.css']
})
export class AutoCompleteComponent implements OnInit {

    @Input('map-autocomplete') isMapAutocomplete: boolean = false;
    @Input('placeholder') placeholder: string;

    @Output('on-place-selected') onPlaceSelected: EventEmitter<any> = new EventEmitter();

    @ViewChild('cont') contEl: any;
    @ViewChild('input') inputEl: any;
    selectedIdx: number = 0;
    public query = '';
    public countries = ["Albania", "Andorra", "Armenia", "Austria", "Azerbaijan", "Belarus",
        "Belgium", "Bosnia & Herzegovina", "Bulgaria", "Croatia", "Cyprus",
        "Czech Republic", "Denmark", "Estonia", "Finland", "France", "Georgia",
        "Germany", "Greece", "Hungary", "Iceland", "Ireland", "Italy", "Kosovo",
        "Latvia", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Malta",
        "Moldova", "Monaco", "Montenegro", "Netherlands", "Norway", "Poland",
        "Portugal", "Romania", "Russia", "San Marino", "Serbia", "Slovakia", "Slovenia",
        "Spain", "Sweden", "Switzerland", "Turkey", "Ukraine", "United Kingdom", "Vatican City"];
    public filteredList = [];
    public elementRef;

    constructor(myElement: ElementRef, private _loader: MapsAPILoader, private _zone: NgZone) {
        this.elementRef = myElement;
    }

    ngOnInit(): void {
        this.autocomplete();
    }

    autocomplete() {
        this._loader.load().then(() => {
            var autocomplete = new google.maps.places.Autocomplete(document.getElementById("autocompleteInput"), {});
            google.maps.event.addListener(autocomplete, 'place_changed', () => {
                this._zone.run(() => {
                    var place = autocomplete.getPlace();
                    this.onPlaceSelected.emit(place);

                    //this.markers.push({
                    //    lat: place.geometry.location.lat(),
                    //    lng: place.geometry.location.lng(),
                    //    label: place.name,
                    //});

                    //this.lat = place.geometry.location.lat();
                    //this.lng = place.geometry.location.lng();

                    //console.log(place);
                });
            });
        });
    }

    handleClick(event) {
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement) {
                inside = true;
            }
            clickedComponent = clickedComponent.parentNode;
        } while (clickedComponent);
        if (!inside) {
            this.filteredList = [];
        }
        this.selectedIdx = 0;
    }

    filter(event: any) {
        if (this.query !== "") {
            this.filteredList = this.countries.filter(function (el) {
                return el.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
            }.bind(this));
            //if (event.code == "ArrowDown" && this.selectedIdx < this.filteredList.length) {
            //    this.selectedIdx++;
            //    this.scrollUL(this.selectedIdx);
            //} else if (event.code == "ArrowUp" && this.selectedIdx > 0) {
            //    this.selectedIdx--;
            //    this.scrollUL(this.selectedIdx);
            //}
        } else {
            this.filteredList = [];
        }
    }

    scrollList() {
        let current = this.contEl.nativeElement.scrollTop;
        let targetLi: HTMLElement = document.getElementById(this.selectedIdx + '');
        this.contEl.nativeElement.scrollTop = 128;// targetLi.offsetTop;
    }

    keyDownReady: boolean = true;
    keyDown(event: any) {
        if (this.filteredList.length > 0 && (event.code == "ArrowDown" || event.code == "ArrowUp")) {
            event.preventDefault();
        }
        if (this.keyDownReady) {
            if (event.code == "ArrowDown" && this.selectedIdx < this.filteredList.length - 1) {
                event.preventDefault();
                this.keyDownReady = false;
                setTimeout(() => {
                    this.keyDownReady = true;
                    this.selectedIdx++;
                    this.scrollUL(this.selectedIdx);
                }, 100);
            } else if (event.code == "ArrowUp" && this.selectedIdx > 0) {
                event.preventDefault();
                this.keyDownReady = false;
                setTimeout(() => {
                    this.keyDownReady = true;
                    this.selectedIdx--;
                    this.scrollUL(this.selectedIdx);
                }, 100);
            }
        }
    }

    scrollUL(li) {
        // scroll UL to make li visible
        // li can be the li element or its id
        if (typeof li !== "object") {
            li = document.getElementById(li);
        }
        if (!li) {
            return;
        }
        var ul = li.parentNode;
        if (!ul) {
            return;
        }
        // fudge adjustment for borders effect on offsetHeight
        var fudge = 4;
        // bottom most position needed for viewing
        var bottom = (ul.scrollTop + (ul.offsetHeight - fudge) - li.offsetHeight);
        // top most position needed for viewing
        var top = ul.scrollTop + fudge;
        if (li.offsetTop <= top) {
            // move to top position if LI above it
            // use algebra to subtract fudge from both sides to solve for ul.scrollTop
            ul.scrollTop = li.offsetTop - fudge;
        } else if (li.offsetTop >= bottom) {
            // move to bottom position if LI below it
            // use algebra to subtract ((ul.offsetHeight - fudge) - li.offsetHeight) from both sides to solve for ul.scrollTop
            ul.scrollTop = li.offsetTop - ((ul.offsetHeight - fudge) - li.offsetHeight);
        }
    };

    select(item) {
        this.query = item;
        this.filteredList = [];
        this.selectedIdx = -1;
    }

    handleBlur(key: any) {
        if (!key || key.keyCode === 13) {
            if (this.selectedIdx > -1) {
                this.query = this.filteredList[this.selectedIdx];
            }
            this.filteredList = [];
            this.selectedIdx = -1;
            let inputEl: HTMLElement = document.getElementById('input');
            inputEl.blur();
        }
    }
}
