﻿import { Component, Input } from '@angular/core';

@Component({
    selector: 'idid-google-maps',
    templateUrl: './google-maps.component.html'
})
export class GoogleMapsComponent {

    @Input('lat') lat: any;
    @Input('lng') lng: any;

    constructor() {
    }

    setCenter(place: any) {
        console.log(place);
    }
}
