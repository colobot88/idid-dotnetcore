﻿import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';

import { GoogleMapsComponent } from './google-maps.component';

@NgModule({
    imports: [
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDjTo5WrhSZel5ZpHtEZ1nCoJ17T3_htn0',
            libraries: ["places"]
        })],
    exports: [GoogleMapsComponent],
    declarations: [GoogleMapsComponent],
    providers: [],
})
export class GoogleMapsModule { }
