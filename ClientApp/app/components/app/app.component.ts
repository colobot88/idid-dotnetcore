import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    title: string = 'My first AGM project';
    lat: number = 51.678418;
    lng: number = 7.809007;
    loggedin: boolean = false;

    constructor(private router: Router) {

    }

    onPlaceSelected(place: any) {
        console.log(place);
    }

    Countries: ['asd', 'bsd'];

    navigate(route: string) {
        this.router.navigate([route]);
    }

    loginWithFacebook() {
        this.loggedin = true;
    }

    logout() {
        this.loggedin = false;
        this.router.navigate(['home']);
    }
}
